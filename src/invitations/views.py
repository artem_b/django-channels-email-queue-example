from django.shortcuts import render
from django.views.generic import CreateView, TemplateView

from .models import Invitation
from .forms import InvitationForm


class InvitationCreateView(CreateView):
    template_name = 'invitations/invitation_create.html'
    form_class = InvitationForm
    model = Invitation
    success_url = '/success/'

class InvitationView(TemplateView):
    template_name = 'invitations/invitation.html'
