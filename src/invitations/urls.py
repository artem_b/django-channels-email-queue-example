from django.conf.urls import url

from .views import InvitationCreateView, InvitationView


urlpatterns = [
    url(
        r'^$',
        InvitationCreateView.as_view(),
        name='invitation_crate',
    ),
    url(
    	r'^success/$',
    	InvitationView.as_view(),
    	name='invitation',
    ),
]